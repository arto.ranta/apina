"use strict";
/**
 * Module dependencies.
 */
const fs = require('fs');
const cors = require('cors');
const helmet = require('helmet');
const express = require('express');

// Load environment variables.
require('dotenv').config();

// Initiate app.
const app = express();

/**
 * Express middleware.
 */
app.use(cors());
app.use(helmet());

app.all('/', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

// Disable HTTP Header Fingerprinting.
app.disable('x-powered-by');

// Set up routes.
require('./routes/index').app(app);

// Set error handler for app.
app.use(function (req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.status(err.status || 500).send({success: false, message: 'Internal Server Error. ' + err.message});
});

const callback = function (port) {
  console.log('The magic happens on port: ' + port);
};

// Set error handler for HTTP server.
const handler = function (err) {
  if (err.errno === 'EADDRINUSE') {
    console.log('port ' + port + ' is in use already');
  } else {
    console.log(err);
  }
};

let server;
let port = process.env.PORT || 3000;

// Start HTTP server.
if (process.env.GREENLOCK_MAINTANER) {
  /**
   * Greenlock Express v4 configuration.
   */
  let config = {sites: []};
  const configDir = './greenlock.d';
  const configFile = configDir + '/config.json';
  if (!fs.existsSync(configDir)) fs.mkdirSync(configDir);
  if (!fs.existsSync(configFile)) fs.writeFileSync(configFile, JSON.stringify(config), 'utf8');

  // Configure domain.
  try {
    config = JSON.parse(fs.readFileSync(configFile).toString());
    if (config.sites.length === 0) {
      config.sites.push({
        subject: process.env.GREENLOCK_DOMAIN,
        altnames: [process.env.GREENLOCK_DOMAIN],
      });
      fs.writeFileSync(configFile, JSON.stringify(config), 'utf8');
    }
  } catch (err) {
    console.log('error', err.message);
  }

  server = require('greenlock-express').init({
    packageRoot: __dirname,
    configDir,
    maintainerEmail: process.env.GREENLOCK_MAINTANER,
    cluster: false,
  }).serve(app);
} else {
  // start HTTP server
  server = require('http').createServer(app)
      // .on('upgrade', require('./app/routes/index').upgrade)
      .listen(port, callback(port))
      .on('error', handler)

}
