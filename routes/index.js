"use strict";
const cors = require('cors');
const router = require('express').Router();
const proxy = require('http-proxy-middleware');

// Load config.
const hosts = require('../config/hosts.json');

const createProxy = (req, res, next) => {
    const target = Object.entries(hosts).find(([key, _value]) => req.originalUrl.includes(key))
    if (!target) {
        return res.status(404).json({ message: 'Not found.' })
    }
    /**
     * Configure proxy middleware
     */
    const http = proxy({
        target: target[1],
        changeOrigin: true,
        /** SSL cert check. */
        secure: false
    });
    return http(req, res, next);
};

/**
 * Proxy module.
 * @module app/routes/proxy
 */
const proxyModule = {
    /**
     * Mount HTTP proxy.
     * @return {object} Router.
     */
    http: () => {
        router.use('', createProxy);
        return router;
    }
}

module.exports = {
    app: (app) => {
        /** Include before other routes. */
        app.options('*', cors());
        app.use('', proxyModule.http());
    }
};
